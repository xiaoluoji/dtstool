﻿Public Class mm_diy   '漠漠DIY功能库
    Dim dm As mycx
    Dim setFindWidth As Integer
    Dim setFindHeight As Integer

    Public Sub New(ByRef mm As mycx, ByVal width As Integer, ByVal height As Integer) '构造函数, 按引用传递一个默默对象进来
        'dm = New mycx   '创建漠漠对象
        dm = mm
        setFindWidth = width
        setFindHeight = height
    End Sub

    Protected Overrides Sub Finalize() '析构函数
        'dm = Nothing
    End Sub

    'Public Sub setPath(ByVal baseDmPath As String)
    '    dm.SetPath(baseDmPath)
    'End Sub

    Public Function bindState(ByVal findHwnd As String) As Integer 'findHwnd 为欲绑定的窗口名称 (HwndStart:启动窗口)/(HwndLogin:登录窗口)/(HwndPlaying:游戏窗口)
        Dim hwnd
        dm.UnBindWindow()
        Select Case findHwnd
            Case "HwndStart"
                hwnd = dm.FindWindowSuper("灵域", 0, 0, "#32770", 2, 0)
                Console.WriteLine("启动窗口: " & hwnd.ToString)
            Case "HwndLogin"
                hwnd = dm.FindWindowSuper("灵域", 0, 0, "QWidget", 2, 0)
                Console.WriteLine("登录窗口: " & hwnd.ToString)
            Case "HwndPlaying"
                hwnd = dm.FindWindowSuper("灵域 ", 0, 1, "TL_LY_WINDOW", 2, 1)
                Console.WriteLine("游戏窗口: " & hwnd.ToString)
        End Select
        If hwnd <> 0 Then
            dm.BindWindowEx(hwnd, "normal", "normal", "normal", "", 101)
            dm.SetWindowState(hwnd, 1)
        End If
        Return hwnd
    End Function ' 循环获取窗口句柄,并激活窗口,返回窗口句柄

    Public Function WaitImg(ByRef imgArr As Hashtable, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer, ByVal print As String, ByVal setImgs As String, ByVal sim As Double, ByVal setTimes As Integer) As Integer
        Dim Id, imgX, imgY   ' Id,imgx, imgY 不能显示定义为 整数类型, 否则漠漠函数传递返回结果不正确!!!!!切记!!!
        Dim i As Integer
        'Console.WriteLine(dm.GetPath())
        For i = 1 To setTimes
            If x1 + y1 + x2 + y2 = 0 Then
                Id = dm.FindPic(0, 0, setFindWidth, setFindHeight, setImgs, "101010", 0.8, 0, imgX, imgY)
            ElseIf x1 + y1 + x2 + y2 <> 0 Then
                Id = dm.FindPic(x1, y1, x2, y2, setImgs, "101010", 0.8, 0, imgX, imgY)
            End If
            If Id <> -1 Then
                imgArr("id") = Id
                imgArr("x") = imgX
                imgArr("y") = imgY
                Console.WriteLine("等图【" & print & "】定时【" & setTimes & "】秒寻找【" & setImgs & "】; 序号【" & imgArr("id") & "】, 坐标 : ImgX=【" & imgArr("x") & "】 ,ImgY=【" & imgArr("y") & "】")
                Return imgArr("id")
            End If
            Console.WriteLine("WaitImg...." & print)
            dm.Delay(1000)
        Next

        Return -1
    End Function '设定时间内找图,直到找到或超时,找到返回图片id(从0开始),没找到返回-1

    Public Function WaitFont(ByRef strArr As Hashtable, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer, ByVal print As String, ByVal setStrs As String, ByVal color As String, ByVal setTimes As Integer) As Integer
        ' setStrs 为要寻找的字, color为颜色串,setTimes 为自定义等待时间(秒)
        Dim Id, strX, strY  ' Id,strx, strY 不能显示定义为 整数类型, 否则漠漠函数传递返回结果不正确!!!!!切记!!!
        Dim i As Integer
        'Console.WriteLine(dm.GetPath())
        For i = 1 To setTimes
            If x1 + y1 + x2 + y2 = 0 Then
                Id = dm.FindStr(0, 0, setFindWidth, setFindHeight, setStrs, color, 1, strX, strY)
            ElseIf x1 + y1 + x2 + y2 <> 0 Then
                Id = dm.FindStr(x1, y1, x2, y2, setStrs, color, 1, strX, strY)
            End If
            If Id <> -1 Then
                strArr("id") = Id
                strArr("x") = strX
                strArr("y") = strY
                Console.WriteLine("自带字库找字【" & print & "】定时【" & setTimes & "】秒寻找【" & setStrs & "】; 序号【" & strArr("id") & "】, 坐标 : strX=【" & strArr("x") & "】 ,strY=【" & strArr("y") & "】")
                Return strArr("id")
            End If
            Console.WriteLine("WaitFont...." & print)
            dm.Delay(1000)
        Next
        Return -1
    End Function '设定时间内【通过自定义字库】找字,直到找到或超时, StrArr[1](从0开始),StrArr[2],StrArr[3]坐标  ,没找到返回-1

    Public Function WaitFontE(ByRef strArr As Hashtable, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer, ByVal print As String, ByVal setStrs As String, ByVal color As String, ByVal fontSize As Integer, ByVal fontFlag As Integer, ByVal setTimes As Integer) As Integer
        ' setStrs 为要寻找的字, color为颜色串, fontSzie为系统字体, fontFlag :0:正常 1:粗体 2:斜体 3:下划线 4:删除线，setTimes 为自定义等待时间(秒)
        Dim i As Integer
        Dim ret, temp  '不要显示定义为 string类型!!!!!
        'Console.WriteLine(dm.GetPath())
        For i = 1 To setTimes
            If x1 + y1 + x2 + y2 = 0 Then
                ret = dm.FindStrWithFontE(0, 0, setFindWidth, setFindHeight, setStrs, color, 1, "宋体", fontSize, fontFlag)
            ElseIf x1 + y1 + x2 + y2 <> 0 Then
                ret = dm.FindStrWithFontE(x1, y1, x2, y2, setStrs, color, 1, "宋体", fontSize, fontFlag)
            End If
            If ret <> "-1|-1|-1" Then
                temp = Split(ret, "|")
                strArr("id") = CInt(temp(0))
                strArr("x") = CInt(temp(1))
                strArr("y") = CInt(temp(2))
                Console.WriteLine("系统字库找字【" & print & "】定时【" & setTimes & "】秒寻找【" & setStrs & "】; 序号【" & strArr("id") & "】, 坐标 : strX=【" & strArr("x") & "】 ,strY=【" & strArr("y") & "】")
                Return strArr("id")
            End If
            Console.WriteLine("WaitFontE...." & print)
            dm.Delay(1000)
        Next

        Return -1
    End Function '设定时间内【系统自带字库】找字,直到找到或超时,找到返回文字  StrArr[1](从0开始),StrArr[2],StrArr[3]坐标  ,没找到返回-1

    Public Function findImg(ByRef imgArr As Hashtable, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer, ByVal print As String, ByVal setImgs As String, ByVal sim As Double) As Integer
        Dim Id, imgX, imgY  ' Id,imgx, imgY 不能显示定义为 整数类型, 否则漠漠函数传递返回结果不正确!!!!!切记!!!
        Dim i As Integer
        'Console.WriteLine(dm.GetPath())
        If x1 + y1 + x2 + y2 = 0 Then
            Id = dm.FindPic(0, 0, setFindWidth, setFindHeight, setImgs, "101010", 0.8, 0, imgX, imgY)
        ElseIf x1 + y1 + x2 + y2 <> 0 Then
            Id = dm.FindPic(x1, y1, x2, y2, setImgs, "101010", 0.8, 0, imgX, imgY)
        End If
        If Id <> -1 Then
            imgArr("id") = Id
            imgArr("x") = imgX
            imgArr("y") = imgY
            Console.WriteLine("等图【" & print & "】图片【" & setImgs & "】; 序号【" & imgArr("id") & "】, 坐标 : ImgX=【" & imgArr("x") & "】 ,ImgY=【" & imgArr("y") & "】")
            Return imgArr("id")
        End If
        Console.WriteLine("findImg...." & print)
        dm.Delay(50)

        Return -1
    End Function '找图,找到返回图片id(从0开始),没找到返回-1

    Public Function findFont(ByRef strArr As Hashtable, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer, ByVal print As String, ByVal setStrs As String, ByVal color As String) As Integer
        ' setStrs 为要寻找的字, color为颜色串,setTimes 为自定义等待时间(秒)
        Dim Id, strX, strY  ' Id,strx, strY 不能显示定义为 整数类型, 否则漠漠函数传递返回结果不正确!!!!!切记!!!
        Dim i As Integer
        'Console.WriteLine(dm.GetPath())
        If x1 + y1 + x2 + y2 = 0 Then
            Id = dm.FindStr(0, 0, setFindWidth, setFindHeight, setStrs, color, 1, strX, strY)
        ElseIf x1 + y1 + x2 + y2 <> 0 Then
            Id = dm.FindStr(x1, y1, x2, y2, setStrs, color, 1, strX, strY)
        End If
        If Id <> -1 Then
            strArr("id") = Id
            strArr("x") = strX
            strArr("y") = strY
            Console.WriteLine("自带字库找字【" & print & "】找字【" & setStrs & "】; 序号【" & strArr("id") & "】, 坐标 : strX=【" & strArr("x") & "】 ,strY=【" & strArr("y") & "】")
            Return strArr("id")
        End If
        Console.WriteLine("findFont...." & print)
        dm.Delay(50)

        Return -1
    End Function '【通过自定义字库】找字, StrArr[1](从0开始),StrArr[2],StrArr[3]坐标  ,没找到返回-1

    Public Function findFontE(ByRef strArr As Hashtable, ByVal x1 As Integer, ByVal y1 As Integer, ByVal x2 As Integer, ByVal y2 As Integer, ByVal print As String, ByVal setStrs As String, ByVal color As String, ByVal fontSize As Integer, ByVal fontFlag As Integer) As Integer
        ' setStrs 为要寻找的字, color为颜色串, fontSzie为系统字体, fontFlag :0:正常 1:粗体 2:斜体 3:下划线 4:删除线，setTimes 为自定义等待时间(秒)
        Dim i As Integer
        Dim ret, temp()  '不要显示定义为 string类型!!!!!
        'Console.WriteLine(dm.GetPath())
        If x1 + y1 + x2 + y2 = 0 Then
            ret = dm.FindStrWithFontE(0, 0, setFindWidth, setFindHeight, setStrs, color, 1, "宋体", fontSize, fontFlag)
        ElseIf x1 + y1 + x2 + y2 <> 0 Then
            ret = dm.FindStrWithFontE(x1, y1, x2, y2, setStrs, color, 1, "宋体", fontSize, fontFlag)
        End If
        If ret <> "-1|-1|-1" Then
            temp = Split(ret, "|")
            strArr("id") = CInt(temp(0))
            strArr("x") = CInt(temp(1))
            strArr("y") = CInt(temp(2))
            Console.WriteLine("系统字库找字【" & print & "】找字【" & setStrs & "】; 序号【" & strArr("id") & "】, 坐标 : strX=【" & strArr("x") & "】 ,strY=【" & strArr("y") & "】")
            Return strArr("id")
        End If
        Console.WriteLine("findFontE...." & print)
        dm.Delay(50)

        Return -1
    End Function '【系统自带字库】找字, StrArr[1](从0开始),StrArr[2],StrArr[3]坐标  ,没找到返回-1

    Public Sub KeyPress(ByVal key_str)
        dm.EnableRealKeypad(1)
        dm.KeyPressStr(key_str, Random(50, 180))
    End Sub    '模拟键盘真实输入

    Public Sub Alt_key(key)
        'E=69(背包)   Z=90(自动战斗)  G=71(收服宝宝)
        dm.KeyDown(18)   '按下ALT键
        dm.Delay(Random(300, 500))
        dm.KeyPress(key)
        dm.Delay(Random(100, 150))
        dm.KeyUp(18)   '松开ALT键
        dm.Delay(Random(1000, 1500))
    End Sub  'Alt快捷键  E=69(背包)   Z=90(自动战斗)  G=71(收服宝宝)

    Public Sub LClick(x, y, w, h, mode)
        'X坐标,   Y坐标,  宽度(从X算起),  高度(从Y算起),  模式: 空=点击后不移动, 1= 点击后移动
        dm.EnableRealMouse(2, 12, 30) ' 开启鼠标动作模拟真实操作
        Select Case mode
            Case 0
                dm.MoveToEx(x, y, w, h)
                dm.Delay(Random(500, 1000))
                dm.LeftDown()
                dm.Delay(Random(30, 80))
                dm.LeftUp()
                dm.Delay(Random(30, 80))
            Case 1
                dm.MoveToEx(x, y, w, h)
                dm.Delay(Random(500, 1000))
                dm.LeftDown()
                dm.Delay(Random(30, 80))
                dm.LeftUp()
                dm.Delay(Random(100, 220))
                dm.MoveToEx(x - 140, y - 110, 80, 80)
        End Select
    End Sub '鼠标真实移动并且单击【左】键或单击【左】键往左上移开鼠标

    Public Sub RClick(x, y, w, h, mode) '鼠标真实移动并且单击【右】键或单击【右】键往左上移开鼠标
        'X坐标,   Y坐标,  宽度(从X算起),  高度(从Y算起),  模式: 空=点击后不移动, 1= 点击后移动
        dm.EnableRealMouse(2, 12, 30) ' 开启鼠标动作模拟真实操作
        Select Case mode
            Case 0
                dm.MoveToEx(x, y, w, h)
                dm.Delay(Random(500, 1000))
                dm.RightDown()
                dm.Delay(Random(30, 80))
                dm.RightUp()
                dm.Delay(Random(30, 80))
            Case 1
                dm.MoveToEx(x, y, w, h)
                dm.Delay(Random(500, 1000))
                dm.RightDown()
                dm.Delay(Random(30, 80))
                dm.RightUp()
                dm.Delay(Random(100, 220))
                dm.MoveToEx(x - 140, y - 140, 80, 80)

        End Select
    End Sub '鼠标真实移动并且单击【左】键或单击【左】键往左上移开鼠标

    Public Function Random(ByVal Min, ByVal Max) As Integer
        Randomize()
        Random = Int((Max - Min + 1) * Rnd() + Min)
    End Function  '通过最小值和最大值得到一个随机数 


End Class
