﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class daTianShi
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblLocalIP = New System.Windows.Forms.Label()
        Me.btnStartRegister = New System.Windows.Forms.Button()
        Me.tboxOutput = New System.Windows.Forms.TextBox()
        Me.radioBaidu = New System.Windows.Forms.RadioButton()
        Me.radio360 = New System.Windows.Forms.RadioButton()
        Me.radioYY = New System.Windows.Forms.RadioButton()
        Me.radio37 = New System.Windows.Forms.RadioButton()
        Me.grpPlatform = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtRegisterNum = New System.Windows.Forms.TextBox()
        Me.gbxExport = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtExportNum = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtGameArea = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxExportPlatform = New System.Windows.Forms.ComboBox()
        Me.btnExportAccount = New System.Windows.Forms.Button()
        Me.tabControl = New System.Windows.Forms.TabControl()
        Me.tabGameAssit = New System.Windows.Forms.TabPage()
        Me.btnImportExchangeDoneAccount = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtExchangeDoneFilePath = New System.Windows.Forms.TextBox()
        Me.btnSelectExchangeDoneFilePath = New System.Windows.Forms.Button()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.btnImportExchangeReadyAccount = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtExchangeReadyFilePath = New System.Windows.Forms.TextBox()
        Me.btnSelectExchangeReadyFilePath = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.tboxGameAssistOutput = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbxSelectPlatform = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnHuanZi = New System.Windows.Forms.Button()
        Me.tabAccount = New System.Windows.Forms.TabPage()
        Me.btnCheckAccount = New System.Windows.Forms.Button()
        Me.btnImportAccount = New System.Windows.Forms.Button()
        Me.gbxImport = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtImportAccountPath = New System.Windows.Forms.TextBox()
        Me.btnImportAccountPath = New System.Windows.Forms.Button()
        Me.lblImportFile = New System.Windows.Forms.Label()
        Me.gbxRegister = New System.Windows.Forms.GroupBox()
        Me.tabDatabase = New System.Windows.Forms.TabPage()
        Me.btnExportByIni = New System.Windows.Forms.Button()
        Me.gbxExportByInifile = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtExportIniPath = New System.Windows.Forms.TextBox()
        Me.btnSelectExportIniPath = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.tboxDbOutput = New System.Windows.Forms.TextBox()
        Me.btnResetArea = New System.Windows.Forms.Button()
        Me.gbxResetArea = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtResetAreaFilePath = New System.Windows.Forms.TextBox()
        Me.btnSelectResetFile = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.tabSetting = New System.Windows.Forms.TabPage()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnSetChromePath = New System.Windows.Forms.Button()
        Me.txtChromePath = New System.Windows.Forms.TextBox()
        Me.btnSaveini = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ckbLog = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDbPasswd = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDbUserName = New System.Windows.Forms.TextBox()
        Me.lblDbUserName = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMysqlIp = New System.Windows.Forms.TextBox()
        Me.txtDbName = New System.Windows.Forms.TextBox()
        Me.lblDbName = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDama2User = New System.Windows.Forms.TextBox()
        Me.lblDama2User = New System.Windows.Forms.Label()
        Me.txtDama2Pwd = New System.Windows.Forms.TextBox()
        Me.lblDama2Pwd = New System.Windows.Forms.Label()
        Me.OpenFileDialogImportFile = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialogChromePath = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialogResetFile = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialogExportIniPath = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialogReadyExchangeFile = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialogExchangeDoneFile = New System.Windows.Forms.OpenFileDialog()
        Me.grpPlatform.SuspendLayout()
        Me.gbxExport.SuspendLayout()
        Me.tabControl.SuspendLayout()
        Me.tabGameAssit.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.tabAccount.SuspendLayout()
        Me.gbxImport.SuspendLayout()
        Me.gbxRegister.SuspendLayout()
        Me.tabDatabase.SuspendLayout()
        Me.gbxExportByInifile.SuspendLayout()
        Me.gbxResetArea.SuspendLayout()
        Me.tabSetting.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblLocalIP
        '
        Me.lblLocalIP.AutoSize = True
        Me.lblLocalIP.Location = New System.Drawing.Point(8, 327)
        Me.lblLocalIP.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblLocalIP.Name = "lblLocalIP"
        Me.lblLocalIP.Size = New System.Drawing.Size(62, 18)
        Me.lblLocalIP.TabIndex = 13
        Me.lblLocalIP.Text = "本机IP"
        '
        'btnStartRegister
        '
        Me.btnStartRegister.Location = New System.Drawing.Point(32, 250)
        Me.btnStartRegister.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnStartRegister.Name = "btnStartRegister"
        Me.btnStartRegister.Size = New System.Drawing.Size(112, 34)
        Me.btnStartRegister.TabIndex = 9
        Me.btnStartRegister.Text = "开始注册"
        Me.btnStartRegister.UseVisualStyleBackColor = True
        '
        'tboxOutput
        '
        Me.tboxOutput.Location = New System.Drawing.Point(10, 381)
        Me.tboxOutput.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tboxOutput.Multiline = True
        Me.tboxOutput.Name = "tboxOutput"
        Me.tboxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tboxOutput.Size = New System.Drawing.Size(968, 148)
        Me.tboxOutput.TabIndex = 14
        '
        'radioBaidu
        '
        Me.radioBaidu.AutoSize = True
        Me.radioBaidu.Location = New System.Drawing.Point(9, 44)
        Me.radioBaidu.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.radioBaidu.Name = "radioBaidu"
        Me.radioBaidu.Size = New System.Drawing.Size(69, 22)
        Me.radioBaidu.TabIndex = 17
        Me.radioBaidu.Text = "百度"
        Me.radioBaidu.UseVisualStyleBackColor = True
        '
        'radio360
        '
        Me.radio360.AutoSize = True
        Me.radio360.Location = New System.Drawing.Point(111, 44)
        Me.radio360.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.radio360.Name = "radio360"
        Me.radio360.Size = New System.Drawing.Size(60, 22)
        Me.radio360.TabIndex = 18
        Me.radio360.Text = "360"
        Me.radio360.UseVisualStyleBackColor = True
        '
        'radioYY
        '
        Me.radioYY.AutoSize = True
        Me.radioYY.Location = New System.Drawing.Point(9, 76)
        Me.radioYY.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.radioYY.Name = "radioYY"
        Me.radioYY.Size = New System.Drawing.Size(51, 22)
        Me.radioYY.TabIndex = 19
        Me.radioYY.Text = "YY"
        Me.radioYY.UseVisualStyleBackColor = True
        '
        'radio37
        '
        Me.radio37.AutoSize = True
        Me.radio37.Checked = True
        Me.radio37.Location = New System.Drawing.Point(202, 44)
        Me.radio37.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.radio37.Name = "radio37"
        Me.radio37.Size = New System.Drawing.Size(78, 22)
        Me.radio37.TabIndex = 20
        Me.radio37.TabStop = True
        Me.radio37.Text = "37wan"
        Me.radio37.UseVisualStyleBackColor = True
        '
        'grpPlatform
        '
        Me.grpPlatform.Controls.Add(Me.radioYY)
        Me.grpPlatform.Controls.Add(Me.radio360)
        Me.grpPlatform.Controls.Add(Me.radio37)
        Me.grpPlatform.Controls.Add(Me.radioBaidu)
        Me.grpPlatform.Location = New System.Drawing.Point(22, 30)
        Me.grpPlatform.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.grpPlatform.Name = "grpPlatform"
        Me.grpPlatform.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.grpPlatform.Size = New System.Drawing.Size(294, 138)
        Me.grpPlatform.TabIndex = 21
        Me.grpPlatform.TabStop = False
        Me.grpPlatform.Text = "注册平台"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 188)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 18)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "注册数量:"
        '
        'txtRegisterNum
        '
        Me.txtRegisterNum.Location = New System.Drawing.Point(117, 183)
        Me.txtRegisterNum.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtRegisterNum.Name = "txtRegisterNum"
        Me.txtRegisterNum.Size = New System.Drawing.Size(198, 28)
        Me.txtRegisterNum.TabIndex = 23
        Me.txtRegisterNum.Text = "10"
        '
        'gbxExport
        '
        Me.gbxExport.Controls.Add(Me.Label7)
        Me.gbxExport.Controls.Add(Me.txtExportNum)
        Me.gbxExport.Controls.Add(Me.Label6)
        Me.gbxExport.Controls.Add(Me.txtGameArea)
        Me.gbxExport.Controls.Add(Me.Label5)
        Me.gbxExport.Controls.Add(Me.Label3)
        Me.gbxExport.Controls.Add(Me.cbxExportPlatform)
        Me.gbxExport.Location = New System.Drawing.Point(356, 10)
        Me.gbxExport.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxExport.Name = "gbxExport"
        Me.gbxExport.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxExport.Size = New System.Drawing.Size(322, 231)
        Me.gbxExport.TabIndex = 24
        Me.gbxExport.TabStop = False
        Me.gbxExport.Text = "导出新账号"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 165)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(278, 18)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "游戏区号可以填写多个,用""|""隔开"
        '
        'txtExportNum
        '
        Me.txtExportNum.Location = New System.Drawing.Point(102, 116)
        Me.txtExportNum.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtExportNum.Name = "txtExportNum"
        Me.txtExportNum.Size = New System.Drawing.Size(205, 28)
        Me.txtExportNum.TabIndex = 5
        Me.txtExportNum.Text = "30"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 120)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 18)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "导出数量"
        '
        'txtGameArea
        '
        Me.txtGameArea.Location = New System.Drawing.Point(102, 74)
        Me.txtGameArea.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtGameArea.Name = "txtGameArea"
        Me.txtGameArea.Size = New System.Drawing.Size(205, 28)
        Me.txtGameArea.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 78)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 18)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "游戏区号"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 42)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 18)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "账号平台"
        '
        'cbxExportPlatform
        '
        Me.cbxExportPlatform.FormattingEnabled = True
        Me.cbxExportPlatform.Items.AddRange(New Object() {"百度", "360", "37wan", "多玩"})
        Me.cbxExportPlatform.Location = New System.Drawing.Point(102, 36)
        Me.cbxExportPlatform.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxExportPlatform.Name = "cbxExportPlatform"
        Me.cbxExportPlatform.Size = New System.Drawing.Size(205, 26)
        Me.cbxExportPlatform.TabIndex = 0
        Me.cbxExportPlatform.Text = "37wan"
        '
        'btnExportAccount
        '
        Me.btnExportAccount.Location = New System.Drawing.Point(566, 250)
        Me.btnExportAccount.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnExportAccount.Name = "btnExportAccount"
        Me.btnExportAccount.Size = New System.Drawing.Size(112, 34)
        Me.btnExportAccount.TabIndex = 6
        Me.btnExportAccount.Text = "导出账号"
        Me.btnExportAccount.UseVisualStyleBackColor = True
        '
        'tabControl
        '
        Me.tabControl.Controls.Add(Me.tabGameAssit)
        Me.tabControl.Controls.Add(Me.tabAccount)
        Me.tabControl.Controls.Add(Me.tabDatabase)
        Me.tabControl.Controls.Add(Me.tabSetting)
        Me.tabControl.Location = New System.Drawing.Point(2, 18)
        Me.tabControl.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabControl.Name = "tabControl"
        Me.tabControl.SelectedIndex = 0
        Me.tabControl.Size = New System.Drawing.Size(1005, 584)
        Me.tabControl.TabIndex = 25
        '
        'tabGameAssit
        '
        Me.tabGameAssit.Controls.Add(Me.btnImportExchangeDoneAccount)
        Me.tabGameAssit.Controls.Add(Me.GroupBox6)
        Me.tabGameAssit.Controls.Add(Me.btnImportExchangeReadyAccount)
        Me.tabGameAssit.Controls.Add(Me.GroupBox5)
        Me.tabGameAssit.Controls.Add(Me.Button2)
        Me.tabGameAssit.Controls.Add(Me.tboxGameAssistOutput)
        Me.tabGameAssit.Controls.Add(Me.GroupBox4)
        Me.tabGameAssit.Location = New System.Drawing.Point(4, 28)
        Me.tabGameAssit.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabGameAssit.Name = "tabGameAssit"
        Me.tabGameAssit.Size = New System.Drawing.Size(997, 552)
        Me.tabGameAssit.TabIndex = 2
        Me.tabGameAssit.Text = "游戏辅助"
        Me.tabGameAssit.UseVisualStyleBackColor = True
        '
        'btnImportExchangeDoneAccount
        '
        Me.btnImportExchangeDoneAccount.Location = New System.Drawing.Point(813, 180)
        Me.btnImportExchangeDoneAccount.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnImportExchangeDoneAccount.Name = "btnImportExchangeDoneAccount"
        Me.btnImportExchangeDoneAccount.Size = New System.Drawing.Size(166, 34)
        Me.btnImportExchangeDoneAccount.TabIndex = 33
        Me.btnImportExchangeDoneAccount.Text = "导入交易完成账号"
        Me.btnImportExchangeDoneAccount.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label19)
        Me.GroupBox6.Controls.Add(Me.txtExchangeDoneFilePath)
        Me.GroupBox6.Controls.Add(Me.btnSelectExchangeDoneFilePath)
        Me.GroupBox6.Controls.Add(Me.Label20)
        Me.GroupBox6.Location = New System.Drawing.Point(656, 10)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox6.Size = New System.Drawing.Size(324, 160)
        Me.GroupBox6.TabIndex = 32
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "交易二:导入交易完成账号"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Red
        Me.Label19.Location = New System.Drawing.Point(6, 92)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(260, 54)
        Me.Label19.TabIndex = 10
        Me.Label19.Text = "注意:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "导入账号格式必须是按照大天使" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "辅助中使用的格式"
        '
        'txtExchangeDoneFilePath
        '
        Me.txtExchangeDoneFilePath.Location = New System.Drawing.Point(9, 56)
        Me.txtExchangeDoneFilePath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtExchangeDoneFilePath.Name = "txtExchangeDoneFilePath"
        Me.txtExchangeDoneFilePath.Size = New System.Drawing.Size(220, 28)
        Me.txtExchangeDoneFilePath.TabIndex = 9
        '
        'btnSelectExchangeDoneFilePath
        '
        Me.btnSelectExchangeDoneFilePath.Location = New System.Drawing.Point(240, 52)
        Me.btnSelectExchangeDoneFilePath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSelectExchangeDoneFilePath.Name = "btnSelectExchangeDoneFilePath"
        Me.btnSelectExchangeDoneFilePath.Size = New System.Drawing.Size(70, 34)
        Me.btnSelectExchangeDoneFilePath.TabIndex = 8
        Me.btnSelectExchangeDoneFilePath.Text = "选择"
        Me.btnSelectExchangeDoneFilePath.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(10, 32)
        Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(188, 18)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "交易完成账号文件路径"
        '
        'btnImportExchangeReadyAccount
        '
        Me.btnImportExchangeReadyAccount.Location = New System.Drawing.Point(474, 180)
        Me.btnImportExchangeReadyAccount.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnImportExchangeReadyAccount.Name = "btnImportExchangeReadyAccount"
        Me.btnImportExchangeReadyAccount.Size = New System.Drawing.Size(166, 34)
        Me.btnImportExchangeReadyAccount.TabIndex = 31
        Me.btnImportExchangeReadyAccount.Text = "导入并分组导出"
        Me.btnImportExchangeReadyAccount.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Controls.Add(Me.txtExchangeReadyFilePath)
        Me.GroupBox5.Controls.Add(Me.btnSelectExchangeReadyFilePath)
        Me.GroupBox5.Controls.Add(Me.Label18)
        Me.GroupBox5.Location = New System.Drawing.Point(316, 10)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox5.Size = New System.Drawing.Size(324, 160)
        Me.GroupBox5.TabIndex = 30
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "交易一:导入需交易账号并分组导出"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Red
        Me.Label17.Location = New System.Drawing.Point(6, 92)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(260, 54)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "注意:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "导入账号格式必须是按照大天使" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "辅助中使用的格式"
        '
        'txtExchangeReadyFilePath
        '
        Me.txtExchangeReadyFilePath.Location = New System.Drawing.Point(9, 56)
        Me.txtExchangeReadyFilePath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtExchangeReadyFilePath.Name = "txtExchangeReadyFilePath"
        Me.txtExchangeReadyFilePath.Size = New System.Drawing.Size(220, 28)
        Me.txtExchangeReadyFilePath.TabIndex = 9
        '
        'btnSelectExchangeReadyFilePath
        '
        Me.btnSelectExchangeReadyFilePath.Location = New System.Drawing.Point(240, 52)
        Me.btnSelectExchangeReadyFilePath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSelectExchangeReadyFilePath.Name = "btnSelectExchangeReadyFilePath"
        Me.btnSelectExchangeReadyFilePath.Size = New System.Drawing.Size(70, 34)
        Me.btnSelectExchangeReadyFilePath.TabIndex = 8
        Me.btnSelectExchangeReadyFilePath.Text = "选择"
        Me.btnSelectExchangeReadyFilePath.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(10, 32)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(170, 18)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "待交易账号文件路径"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(834, 360)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(159, 34)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "测试Cookies"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'tboxGameAssistOutput
        '
        Me.tboxGameAssistOutput.Location = New System.Drawing.Point(10, 404)
        Me.tboxGameAssistOutput.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tboxGameAssistOutput.Multiline = True
        Me.tboxGameAssistOutput.Name = "tboxGameAssistOutput"
        Me.tboxGameAssistOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tboxGameAssistOutput.Size = New System.Drawing.Size(968, 132)
        Me.tboxGameAssistOutput.TabIndex = 3
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.cbxSelectPlatform)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.btnHuanZi)
        Me.GroupBox4.Location = New System.Drawing.Point(10, 10)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox4.Size = New System.Drawing.Size(297, 160)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "换字"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(10, 69)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(89, 18)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "账号平台:"
        '
        'cbxSelectPlatform
        '
        Me.cbxSelectPlatform.FormattingEnabled = True
        Me.cbxSelectPlatform.Items.AddRange(New Object() {"百度", "360", "37wan", "多玩"})
        Me.cbxSelectPlatform.Location = New System.Drawing.Point(100, 64)
        Me.cbxSelectPlatform.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.cbxSelectPlatform.Name = "cbxSelectPlatform"
        Me.cbxSelectPlatform.Size = New System.Drawing.Size(180, 26)
        Me.cbxSelectPlatform.TabIndex = 3
        Me.cbxSelectPlatform.Text = "37wan"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(10, 32)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(251, 18)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "选择要换字的平台和游戏区号:"
        '
        'btnHuanZi
        '
        Me.btnHuanZi.Location = New System.Drawing.Point(178, 104)
        Me.btnHuanZi.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnHuanZi.Name = "btnHuanZi"
        Me.btnHuanZi.Size = New System.Drawing.Size(104, 34)
        Me.btnHuanZi.TabIndex = 1
        Me.btnHuanZi.Text = "开始换字"
        Me.btnHuanZi.UseVisualStyleBackColor = True
        '
        'tabAccount
        '
        Me.tabAccount.Controls.Add(Me.btnCheckAccount)
        Me.tabAccount.Controls.Add(Me.btnImportAccount)
        Me.tabAccount.Controls.Add(Me.gbxImport)
        Me.tabAccount.Controls.Add(Me.gbxRegister)
        Me.tabAccount.Controls.Add(Me.btnExportAccount)
        Me.tabAccount.Controls.Add(Me.btnStartRegister)
        Me.tabAccount.Controls.Add(Me.gbxExport)
        Me.tabAccount.Controls.Add(Me.lblLocalIP)
        Me.tabAccount.Controls.Add(Me.tboxOutput)
        Me.tabAccount.Location = New System.Drawing.Point(4, 28)
        Me.tabAccount.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabAccount.Name = "tabAccount"
        Me.tabAccount.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabAccount.Size = New System.Drawing.Size(997, 552)
        Me.tabAccount.TabIndex = 0
        Me.tabAccount.Text = "账号管理"
        Me.tabAccount.UseVisualStyleBackColor = True
        '
        'btnCheckAccount
        '
        Me.btnCheckAccount.Location = New System.Drawing.Point(225, 250)
        Me.btnCheckAccount.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCheckAccount.Name = "btnCheckAccount"
        Me.btnCheckAccount.Size = New System.Drawing.Size(112, 34)
        Me.btnCheckAccount.TabIndex = 28
        Me.btnCheckAccount.Text = "检查账号"
        Me.btnCheckAccount.UseVisualStyleBackColor = True
        '
        'btnImportAccount
        '
        Me.btnImportAccount.Location = New System.Drawing.Point(872, 250)
        Me.btnImportAccount.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnImportAccount.Name = "btnImportAccount"
        Me.btnImportAccount.Size = New System.Drawing.Size(112, 34)
        Me.btnImportAccount.TabIndex = 27
        Me.btnImportAccount.Text = "导入账号"
        Me.btnImportAccount.UseVisualStyleBackColor = True
        '
        'gbxImport
        '
        Me.gbxImport.Controls.Add(Me.Label8)
        Me.gbxImport.Controls.Add(Me.txtImportAccountPath)
        Me.gbxImport.Controls.Add(Me.btnImportAccountPath)
        Me.gbxImport.Controls.Add(Me.lblImportFile)
        Me.gbxImport.Location = New System.Drawing.Point(688, 10)
        Me.gbxImport.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxImport.Name = "gbxImport"
        Me.gbxImport.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxImport.Size = New System.Drawing.Size(300, 231)
        Me.gbxImport.TabIndex = 26
        Me.gbxImport.TabStop = False
        Me.gbxImport.Text = "导入账号 "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(6, 120)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(278, 72)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "注意:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "导入账号格式必须是按照大天使辅" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "助中使用的格式"
        '
        'txtImportAccountPath
        '
        Me.txtImportAccountPath.Location = New System.Drawing.Point(9, 74)
        Me.txtImportAccountPath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtImportAccountPath.Name = "txtImportAccountPath"
        Me.txtImportAccountPath.Size = New System.Drawing.Size(200, 28)
        Me.txtImportAccountPath.TabIndex = 9
        '
        'btnImportAccountPath
        '
        Me.btnImportAccountPath.Location = New System.Drawing.Point(220, 69)
        Me.btnImportAccountPath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnImportAccountPath.Name = "btnImportAccountPath"
        Me.btnImportAccountPath.Size = New System.Drawing.Size(70, 34)
        Me.btnImportAccountPath.TabIndex = 8
        Me.btnImportAccountPath.Text = "选择"
        Me.btnImportAccountPath.UseVisualStyleBackColor = True
        '
        'lblImportFile
        '
        Me.lblImportFile.AutoSize = True
        Me.lblImportFile.Location = New System.Drawing.Point(10, 42)
        Me.lblImportFile.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblImportFile.Name = "lblImportFile"
        Me.lblImportFile.Size = New System.Drawing.Size(116, 18)
        Me.lblImportFile.TabIndex = 0
        Me.lblImportFile.Text = "导入文件路径"
        '
        'gbxRegister
        '
        Me.gbxRegister.Controls.Add(Me.grpPlatform)
        Me.gbxRegister.Controls.Add(Me.Label2)
        Me.gbxRegister.Controls.Add(Me.txtRegisterNum)
        Me.gbxRegister.Location = New System.Drawing.Point(9, 10)
        Me.gbxRegister.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxRegister.Name = "gbxRegister"
        Me.gbxRegister.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxRegister.Size = New System.Drawing.Size(328, 231)
        Me.gbxRegister.TabIndex = 25
        Me.gbxRegister.TabStop = False
        Me.gbxRegister.Text = "注册账号"
        '
        'tabDatabase
        '
        Me.tabDatabase.Controls.Add(Me.btnExportByIni)
        Me.tabDatabase.Controls.Add(Me.gbxExportByInifile)
        Me.tabDatabase.Controls.Add(Me.tboxDbOutput)
        Me.tabDatabase.Controls.Add(Me.btnResetArea)
        Me.tabDatabase.Controls.Add(Me.gbxResetArea)
        Me.tabDatabase.Location = New System.Drawing.Point(4, 28)
        Me.tabDatabase.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabDatabase.Name = "tabDatabase"
        Me.tabDatabase.Size = New System.Drawing.Size(997, 552)
        Me.tabDatabase.TabIndex = 3
        Me.tabDatabase.Text = "数据库管理"
        Me.tabDatabase.UseVisualStyleBackColor = True
        '
        'btnExportByIni
        '
        Me.btnExportByIni.Location = New System.Drawing.Point(498, 312)
        Me.btnExportByIni.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnExportByIni.Name = "btnExportByIni"
        Me.btnExportByIni.Size = New System.Drawing.Size(138, 34)
        Me.btnExportByIni.TabIndex = 32
        Me.btnExportByIni.Text = "导出账号"
        Me.btnExportByIni.UseVisualStyleBackColor = True
        '
        'gbxExportByInifile
        '
        Me.gbxExportByInifile.Controls.Add(Me.Label15)
        Me.gbxExportByInifile.Controls.Add(Me.txtExportIniPath)
        Me.gbxExportByInifile.Controls.Add(Me.btnSelectExportIniPath)
        Me.gbxExportByInifile.Controls.Add(Me.Label16)
        Me.gbxExportByInifile.Location = New System.Drawing.Point(336, 4)
        Me.gbxExportByInifile.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxExportByInifile.Name = "gbxExportByInifile"
        Me.gbxExportByInifile.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxExportByInifile.Size = New System.Drawing.Size(300, 298)
        Me.gbxExportByInifile.TabIndex = 31
        Me.gbxExportByInifile.TabStop = False
        Me.gbxExportByInifile.Text = "通过配置文件导出账号"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Red
        Me.Label15.Location = New System.Drawing.Point(12, 122)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(251, 162)
        Me.Label15.TabIndex = 10
        Me.Label15.Text = "注意:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "配置文件格式为" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "hp-16-1|37wan|3700,3701|17" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "第一列是机器名" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "第二例是游平台" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "第三列是需要挂机的游戏区号," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "多个逗" & _
    "号隔开" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "第四列是挂机的数量"
        '
        'txtExportIniPath
        '
        Me.txtExportIniPath.Location = New System.Drawing.Point(9, 74)
        Me.txtExportIniPath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtExportIniPath.Name = "txtExportIniPath"
        Me.txtExportIniPath.Size = New System.Drawing.Size(200, 28)
        Me.txtExportIniPath.TabIndex = 9
        '
        'btnSelectExportIniPath
        '
        Me.btnSelectExportIniPath.Location = New System.Drawing.Point(220, 69)
        Me.btnSelectExportIniPath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSelectExportIniPath.Name = "btnSelectExportIniPath"
        Me.btnSelectExportIniPath.Size = New System.Drawing.Size(70, 34)
        Me.btnSelectExportIniPath.TabIndex = 8
        Me.btnSelectExportIniPath.Text = "选择"
        Me.btnSelectExportIniPath.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(10, 42)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(116, 18)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "导出配置文件"
        '
        'tboxDbOutput
        '
        Me.tboxDbOutput.Location = New System.Drawing.Point(10, 387)
        Me.tboxDbOutput.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tboxDbOutput.Multiline = True
        Me.tboxDbOutput.Name = "tboxDbOutput"
        Me.tboxDbOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tboxDbOutput.Size = New System.Drawing.Size(968, 148)
        Me.tboxDbOutput.TabIndex = 30
        '
        'btnResetArea
        '
        Me.btnResetArea.Location = New System.Drawing.Point(172, 312)
        Me.btnResetArea.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnResetArea.Name = "btnResetArea"
        Me.btnResetArea.Size = New System.Drawing.Size(138, 34)
        Me.btnResetArea.TabIndex = 29
        Me.btnResetArea.Text = "重置游戏区号"
        Me.btnResetArea.UseVisualStyleBackColor = True
        '
        'gbxResetArea
        '
        Me.gbxResetArea.Controls.Add(Me.Label13)
        Me.gbxResetArea.Controls.Add(Me.txtResetAreaFilePath)
        Me.gbxResetArea.Controls.Add(Me.btnSelectResetFile)
        Me.gbxResetArea.Controls.Add(Me.Label14)
        Me.gbxResetArea.Location = New System.Drawing.Point(10, 4)
        Me.gbxResetArea.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxResetArea.Name = "gbxResetArea"
        Me.gbxResetArea.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxResetArea.Size = New System.Drawing.Size(300, 298)
        Me.gbxResetArea.TabIndex = 28
        Me.gbxResetArea.TabStop = False
        Me.gbxResetArea.Text = "重置游戏区号"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("宋体", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(14, 150)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(241, 105)
        Me.Label13.TabIndex = 10
        Me.Label13.Text = "注意:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "导入账号格式必须是按照" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "大天使辅助中使用的格式"
        '
        'txtResetAreaFilePath
        '
        Me.txtResetAreaFilePath.Location = New System.Drawing.Point(9, 74)
        Me.txtResetAreaFilePath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtResetAreaFilePath.Name = "txtResetAreaFilePath"
        Me.txtResetAreaFilePath.Size = New System.Drawing.Size(200, 28)
        Me.txtResetAreaFilePath.TabIndex = 9
        '
        'btnSelectResetFile
        '
        Me.btnSelectResetFile.Location = New System.Drawing.Point(220, 69)
        Me.btnSelectResetFile.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSelectResetFile.Name = "btnSelectResetFile"
        Me.btnSelectResetFile.Size = New System.Drawing.Size(70, 34)
        Me.btnSelectResetFile.TabIndex = 8
        Me.btnSelectResetFile.Text = "选择"
        Me.btnSelectResetFile.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(10, 42)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(134, 18)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "待重置账号文件"
        '
        'tabSetting
        '
        Me.tabSetting.Controls.Add(Me.Label12)
        Me.tabSetting.Controls.Add(Me.btnSetChromePath)
        Me.tabSetting.Controls.Add(Me.txtChromePath)
        Me.tabSetting.Controls.Add(Me.btnSaveini)
        Me.tabSetting.Controls.Add(Me.GroupBox3)
        Me.tabSetting.Controls.Add(Me.GroupBox1)
        Me.tabSetting.Controls.Add(Me.GroupBox2)
        Me.tabSetting.Location = New System.Drawing.Point(4, 28)
        Me.tabSetting.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabSetting.Name = "tabSetting"
        Me.tabSetting.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tabSetting.Size = New System.Drawing.Size(997, 552)
        Me.tabSetting.TabIndex = 1
        Me.tabSetting.Text = "设置"
        Me.tabSetting.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(10, 360)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(152, 18)
        Me.Label12.TabIndex = 23
        Me.Label12.Text = "Chrome浏览器路径"
        '
        'btnSetChromePath
        '
        Me.btnSetChromePath.Location = New System.Drawing.Point(825, 352)
        Me.btnSetChromePath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSetChromePath.Name = "btnSetChromePath"
        Me.btnSetChromePath.Size = New System.Drawing.Size(70, 34)
        Me.btnSetChromePath.TabIndex = 22
        Me.btnSetChromePath.Text = "选择"
        Me.btnSetChromePath.UseVisualStyleBackColor = True
        '
        'txtChromePath
        '
        Me.txtChromePath.Location = New System.Drawing.Point(162, 356)
        Me.txtChromePath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtChromePath.Name = "txtChromePath"
        Me.txtChromePath.Size = New System.Drawing.Size(640, 28)
        Me.txtChromePath.TabIndex = 21
        '
        'btnSaveini
        '
        Me.btnSaveini.Location = New System.Drawing.Point(783, 444)
        Me.btnSaveini.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnSaveini.Name = "btnSaveini"
        Me.btnSaveini.Size = New System.Drawing.Size(112, 34)
        Me.btnSaveini.TabIndex = 20
        Me.btnSaveini.Text = "保存配置"
        Me.btnSaveini.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ckbLog)
        Me.GroupBox3.Location = New System.Drawing.Point(20, 254)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox3.Size = New System.Drawing.Size(876, 72)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "其他"
        '
        'ckbLog
        '
        Me.ckbLog.AutoSize = True
        Me.ckbLog.Location = New System.Drawing.Point(33, 32)
        Me.ckbLog.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ckbLog.Name = "ckbLog"
        Me.ckbLog.Size = New System.Drawing.Size(106, 22)
        Me.ckbLog.TabIndex = 0
        Me.ckbLog.Text = "开启日志"
        Me.ckbLog.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDbPasswd)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtDbUserName)
        Me.GroupBox1.Controls.Add(Me.lblDbUserName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtMysqlIp)
        Me.GroupBox1.Controls.Add(Me.txtDbName)
        Me.GroupBox1.Controls.Add(Me.lblDbName)
        Me.GroupBox1.Location = New System.Drawing.Point(20, 21)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(488, 206)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "数据库设置"
        '
        'txtDbPasswd
        '
        Me.txtDbPasswd.Location = New System.Drawing.Point(142, 164)
        Me.txtDbPasswd.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDbPasswd.Name = "txtDbPasswd"
        Me.txtDbPasswd.Size = New System.Drawing.Size(320, 28)
        Me.txtDbPasswd.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 168)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 18)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "数据库密码"
        '
        'txtDbUserName
        '
        Me.txtDbUserName.Location = New System.Drawing.Point(142, 118)
        Me.txtDbUserName.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDbUserName.Name = "txtDbUserName"
        Me.txtDbUserName.Size = New System.Drawing.Size(320, 28)
        Me.txtDbUserName.TabIndex = 7
        '
        'lblDbUserName
        '
        Me.lblDbUserName.AutoSize = True
        Me.lblDbUserName.Location = New System.Drawing.Point(8, 123)
        Me.lblDbUserName.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDbUserName.Name = "lblDbUserName"
        Me.lblDbUserName.Size = New System.Drawing.Size(116, 18)
        Me.lblDbUserName.TabIndex = 6
        Me.lblDbUserName.Text = "数据库用户名"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 36)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 18)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Mysql服务器IP"
        '
        'txtMysqlIp
        '
        Me.txtMysqlIp.Location = New System.Drawing.Point(142, 32)
        Me.txtMysqlIp.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtMysqlIp.Name = "txtMysqlIp"
        Me.txtMysqlIp.Size = New System.Drawing.Size(320, 28)
        Me.txtMysqlIp.TabIndex = 5
        Me.txtMysqlIp.Text = "127.0.0.1"
        '
        'txtDbName
        '
        Me.txtDbName.Location = New System.Drawing.Point(142, 75)
        Me.txtDbName.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDbName.Name = "txtDbName"
        Me.txtDbName.Size = New System.Drawing.Size(320, 28)
        Me.txtDbName.TabIndex = 3
        '
        'lblDbName
        '
        Me.lblDbName.AutoSize = True
        Me.lblDbName.Location = New System.Drawing.Point(8, 80)
        Me.lblDbName.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDbName.Name = "lblDbName"
        Me.lblDbName.Size = New System.Drawing.Size(98, 18)
        Me.lblDbName.TabIndex = 1
        Me.lblDbName.Text = "数据库名字"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtDama2User)
        Me.GroupBox2.Controls.Add(Me.lblDama2User)
        Me.GroupBox2.Controls.Add(Me.txtDama2Pwd)
        Me.GroupBox2.Controls.Add(Me.lblDama2Pwd)
        Me.GroupBox2.Location = New System.Drawing.Point(534, 21)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(362, 206)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "打码兔设置"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(16, 118)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(134, 34)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "查询剩余题分"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 168)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(89, 18)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "剩余题分:"
        '
        'txtDama2User
        '
        Me.txtDama2User.Location = New System.Drawing.Point(66, 22)
        Me.txtDama2User.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDama2User.Name = "txtDama2User"
        Me.txtDama2User.Size = New System.Drawing.Size(264, 28)
        Me.txtDama2User.TabIndex = 4
        '
        'lblDama2User
        '
        Me.lblDama2User.AutoSize = True
        Me.lblDama2User.Location = New System.Drawing.Point(14, 28)
        Me.lblDama2User.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDama2User.Name = "lblDama2User"
        Me.lblDama2User.Size = New System.Drawing.Size(44, 18)
        Me.lblDama2User.TabIndex = 3
        Me.lblDama2User.Text = "账号"
        '
        'txtDama2Pwd
        '
        Me.txtDama2Pwd.Location = New System.Drawing.Point(66, 66)
        Me.txtDama2Pwd.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtDama2Pwd.Name = "txtDama2Pwd"
        Me.txtDama2Pwd.Size = New System.Drawing.Size(264, 28)
        Me.txtDama2Pwd.TabIndex = 2
        '
        'lblDama2Pwd
        '
        Me.lblDama2Pwd.AutoSize = True
        Me.lblDama2Pwd.Location = New System.Drawing.Point(14, 75)
        Me.lblDama2Pwd.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDama2Pwd.Name = "lblDama2Pwd"
        Me.lblDama2Pwd.Size = New System.Drawing.Size(44, 18)
        Me.lblDama2Pwd.TabIndex = 0
        Me.lblDama2Pwd.Text = "密码"
        '
        'OpenFileDialogImportFile
        '
        Me.OpenFileDialogImportFile.Filter = "文本文件(*.txt)|*.txt"
        '
        'OpenFileDialogChromePath
        '
        Me.OpenFileDialogChromePath.Filter = "可执行文件(*.exe)|*.exe"
        '
        'OpenFileDialogResetFile
        '
        Me.OpenFileDialogResetFile.Filter = "文本文件(*.txt)|*.txt"
        '
        'OpenFileDialogExportIniPath
        '
        Me.OpenFileDialogExportIniPath.Filter = "文本文件(*.txt)|*.txt"
        '
        'OpenFileDialogReadyExchangeFile
        '
        Me.OpenFileDialogReadyExchangeFile.Filter = "文本文件(*.txt)|*.txt"
        '
        'OpenFileDialogExchangeDoneFile
        '
        Me.OpenFileDialogExchangeDoneFile.Filter = "文本文件(*.txt)|*.txt"
        '
        'daTianShi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1006, 606)
        Me.Controls.Add(Me.tabControl)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "daTianShi"
        Me.Text = "大天使辅助工具"
        Me.grpPlatform.ResumeLayout(False)
        Me.grpPlatform.PerformLayout()
        Me.gbxExport.ResumeLayout(False)
        Me.gbxExport.PerformLayout()
        Me.tabControl.ResumeLayout(False)
        Me.tabGameAssit.ResumeLayout(False)
        Me.tabGameAssit.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.tabAccount.ResumeLayout(False)
        Me.tabAccount.PerformLayout()
        Me.gbxImport.ResumeLayout(False)
        Me.gbxImport.PerformLayout()
        Me.gbxRegister.ResumeLayout(False)
        Me.gbxRegister.PerformLayout()
        Me.tabDatabase.ResumeLayout(False)
        Me.tabDatabase.PerformLayout()
        Me.gbxExportByInifile.ResumeLayout(False)
        Me.gbxExportByInifile.PerformLayout()
        Me.gbxResetArea.ResumeLayout(False)
        Me.gbxResetArea.PerformLayout()
        Me.tabSetting.ResumeLayout(False)
        Me.tabSetting.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLocalIP As System.Windows.Forms.Label
    Friend WithEvents btnStartRegister As System.Windows.Forms.Button
    Friend WithEvents tboxOutput As System.Windows.Forms.TextBox
    Friend WithEvents radioBaidu As System.Windows.Forms.RadioButton
    Friend WithEvents radio360 As System.Windows.Forms.RadioButton
    Friend WithEvents radioYY As System.Windows.Forms.RadioButton
    Friend WithEvents radio37 As System.Windows.Forms.RadioButton
    Friend WithEvents grpPlatform As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtRegisterNum As System.Windows.Forms.TextBox
    Friend WithEvents gbxExport As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxExportPlatform As System.Windows.Forms.ComboBox
    Friend WithEvents txtGameArea As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtExportNum As System.Windows.Forms.TextBox
    Friend WithEvents btnExportAccount As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tabControl As System.Windows.Forms.TabControl
    Friend WithEvents tabAccount As System.Windows.Forms.TabPage
    Friend WithEvents gbxRegister As System.Windows.Forms.GroupBox
    Friend WithEvents tabSetting As System.Windows.Forms.TabPage
    Friend WithEvents btnSaveini As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ckbLog As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDbPasswd As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDbUserName As System.Windows.Forms.TextBox
    Friend WithEvents lblDbUserName As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMysqlIp As System.Windows.Forms.TextBox
    Friend WithEvents txtDbName As System.Windows.Forms.TextBox
    Friend WithEvents lblDbName As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDama2User As System.Windows.Forms.TextBox
    Friend WithEvents lblDama2User As System.Windows.Forms.Label
    Friend WithEvents txtDama2Pwd As System.Windows.Forms.TextBox
    Friend WithEvents lblDama2Pwd As System.Windows.Forms.Label
    Friend WithEvents gbxImport As System.Windows.Forms.GroupBox
    Friend WithEvents lblImportFile As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialogImportFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtImportAccountPath As System.Windows.Forms.TextBox
    Friend WithEvents btnImportAccountPath As System.Windows.Forms.Button
    Friend WithEvents btnImportAccount As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tabGameAssit As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnHuanZi As System.Windows.Forms.Button
    Friend WithEvents cbxSelectPlatform As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tboxGameAssistOutput As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnSetChromePath As System.Windows.Forms.Button
    Friend WithEvents txtChromePath As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialogChromePath As System.Windows.Forms.OpenFileDialog
    Friend WithEvents tabDatabase As System.Windows.Forms.TabPage
    Friend WithEvents btnResetArea As System.Windows.Forms.Button
    Friend WithEvents gbxResetArea As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtResetAreaFilePath As System.Windows.Forms.TextBox
    Friend WithEvents btnSelectResetFile As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialogResetFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents tboxDbOutput As System.Windows.Forms.TextBox
    Friend WithEvents btnExportByIni As System.Windows.Forms.Button
    Friend WithEvents gbxExportByInifile As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtExportIniPath As System.Windows.Forms.TextBox
    Friend WithEvents btnSelectExportIniPath As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialogExportIniPath As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnCheckAccount As System.Windows.Forms.Button
    Friend WithEvents btnImportExchangeReadyAccount As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtExchangeReadyFilePath As System.Windows.Forms.TextBox
    Friend WithEvents btnSelectExchangeReadyFilePath As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnImportExchangeDoneAccount As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtExchangeDoneFilePath As System.Windows.Forms.TextBox
    Friend WithEvents btnSelectExchangeDoneFilePath As System.Windows.Forms.Button
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialogReadyExchangeFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenFileDialogExchangeDoneFile As System.Windows.Forms.OpenFileDialog

End Class
